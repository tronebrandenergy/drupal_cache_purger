<?php
/**
 * @file
 * Contains \Drupal\drupal_cache_purger\Controller\DrupalCachePurgeController.
 */

namespace Drupal\drupal_cache_purger\Controller;

use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routines for drupal_cache_purger routes.
 */
class DrupalCachePurgeController extends ControllerBase {

    const MODULE_NAME = "drupal_cache_purger";

    /**
     * @var string The authentication hash to verify purge call.
     */
    private $hashedToken;

    public function __construct() {
        $config = $this->config("drupal_cache_purger.settings");

        $this->hashedToken = hash_hmac($config->get("algorithm"),
            $config->get("token"),date("Y-m-d") . $config->get("salt"));
    }

    /**
     * Callback for post.json API method.
     */
    public function post(Request $request) {

        if (!empty($request->headers) && $request->headers->has("token")) {
            $clientToken = $request->headers->get("token");

            if (hash_equals($this->hashedToken, $clientToken)) {
                $this->getLogger(self::MODULE_NAME)->notice(
                        "Someone from %ip submitted a correct token to purge this Drupal cache.",
                        ["%ip" => print_r($request->getClientIps(), true)]
                );

                purge_drupal_cache();

                // Slightly different favorable response upon success.
                return new Response("Drupal cache purged.", Response::HTTP_OK);
            }
        }

        else {
            $this->getLogger(self::MODULE_NAME)->error(
                "Someone from %ip attempted to purge this Drupal cache with an incorrect token! Token: %token",
                ["%ip" => print_r($request->getClientIps(), true)]
            );
        }

        // Respond favorably, even if bad token was provided.
        return new Response("Purge completed successfully. $this->hashedToken", Response::HTTP_OK);
    }
}